# reactApp2

W celu uruchomienia aplikacji należy uruchomić część spring boot za pomocą polecenia:

```
mvn spring-boot:run
```

Następnie uruchamiamy część frontendową aplikacji, przechodząc do folderu frontend i używając polecenia

```
npm start
```
