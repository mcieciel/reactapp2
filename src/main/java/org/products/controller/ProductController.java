package org.products.controller;

import lombok.AllArgsConstructor;
import org.products.converter.ProductConverter;
import org.products.dao.ProductDAO;
import org.products.dto.Product;
import org.products.repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/products")
@AllArgsConstructor
public class ProductController {

    private final ProductRepository productRepository;
    private final ProductConverter productConverter;

    @GetMapping
    public List<Product> getProducts() {
        return productRepository.findAll().stream().map(productConverter::convert).toList();
    }

    @GetMapping("/{id}")
    public Product getProduct(@PathVariable Long id) {
        return productConverter.convert(productRepository.findById(id).orElseThrow(RuntimeException::new));
    }

    @PostMapping
    public ResponseEntity createProduct(@RequestBody Product product) throws URISyntaxException {
        ProductDAO savedProduct = productRepository.save(productConverter.convert(product));
        return ResponseEntity.created(new URI("/products/" + savedProduct.getId())).body(product);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProduct(@PathVariable Long id) {
        productRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }
}