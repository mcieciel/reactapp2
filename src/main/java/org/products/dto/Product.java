package org.products.dto;

import jakarta.validation.constraints.NotNull;
import lombok.*;

@Data
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    private Long id;
    @NotNull
    private String name;
    @NotNull
    private Double price;
}
