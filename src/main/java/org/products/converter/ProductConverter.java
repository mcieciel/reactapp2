package org.products.converter;

import org.products.dao.ProductDAO;
import org.products.dto.Product;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class ProductConverter implements Converter<ProductDAO, Product> {

    @Override
    public Product convert(ProductDAO source) {
        return Product.builder()
                .id(source.getId())
                .name(source.getName())
                .price(source.getPrice())
                .build();
    }

    public ProductDAO convert(Product source) {
        return ProductDAO.builder()
                .id(source.getId())
                .name(source.getName())
                .price(source.getPrice())
                .build();
    }
}
